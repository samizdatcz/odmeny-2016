library(plyr)

rm(list=ls())

df <- read.csv('./data/datavnitro.csv', encoding='UTF-8')



### pyramidátor (do šířky uspořádaně)

sirka <- 10

df$x <- 0
df$y <- df$prijmy/1000000

dffin <- df[0,]

ministerstva <- unique(df$ministerstvo) 

for(j in ministerstva) {
  
  print(j)
  
  dfmin <- df[df$ministerstvo == j,]
  
  dfmin <- dfmin[order(dfmin$prijmy, decreasing=T),]
  
  i <- 1
  while (i <= nrow(dfmin)) {

    rada <- dfmin[i,]['y']
    rada <- dfmin[dfmin$y >= as.numeric(rada-0.05),] 
    rada <- rada[1:min(nrow(rada), 5),]
    peers <- nrow(rada)
    lead <- round_any(100*max(rada$y), 5, f = ceiling)
    if(i>1) if(lead >= dffin[nrow(dffin),]['y']) lead <- lead-5
 
    for(h in 1:peers) {
   
      rada[h,]['x'] <- (-peers+1)*sirka/2 + sirka*(h-1)
      rada[h,]['y'] <- lead

    }
    
    dffin <- rbind(dffin, rada)
    dfmin[i:(i+peers-1),]['y'] <- 0
    i <- i + nrow(rada)
    
  }
  
}

write.csv(dffin, file='./data/datavnitroex.csv', row.names=F, fileEncoding='UTF-8')


### kolečkátor (do šířky)

df$x <- 0
df$y <- df$prijmy

dffin <- df[0,]

ministerstva <- unique(df$ministerstvo) 

for(j in ministerstva) {
  
  print(j)
  
  dfmin <- df[df$ministerstvo == j,]
  
  for(i in 1:nrow(dfmin)) {
    for(h in 1:nrow(dfmin)) {
      if(i != h) {
        if(abs(dfmin[i,]['y'] - dfmin[h,]['y']) <= 0.05 & abs(dfmin[i,]['x'] - dfmin[h,]['x']) <= 0.05) {
          dfmin[i,]['y'] <- dfmin[i,]['y'] + round(runif(1, -10, 10))/100;
          dfmin[i,]['x'] <- dfmin[i,]['x'] + round(runif(1, -10, 10))/100;
          if(dfmin[i,]['x'] >= 0.1) dfmin[i,]['x'] <- round(runif(1, -10, 10))/100 
        }
      }
    }
  }
  
  dffin <- rbind(dffin, dfmin)
}

write.csv(dffin, file='./data/data.csv', row.names=F, fileEncoding='UTF-8')



### čtverečkátor (pod sebe)

df$y <- df$prijmy

dffin <- df[0,]

ministerstva <- unique(df$ministerstvo) 

for(j in ministerstva) {
  
  dfmin <- df[df$ministerstvo == j,]
    
  dfmin <- dfmin[order(dfmin$prijmy, decreasing=T),]

  for(i in 1:(nrow(dfmin)-1)) {
    if(dfmin$y[i]-0.02 < dfmin$y[i+1]) {
      dfmin$y[i+1] <- dfmin$y[i]-0.02
    }
  }

  dffin <- rbind(dffin, dfmin)
}

write.csv(dffin, file='./data/data.csv', row.names=F, fileEncoding='UTF-8')
